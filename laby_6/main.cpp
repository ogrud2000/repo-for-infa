#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "winbgi2.h"
#define M 31

double gauss(double a[][M], double b[], int n, double x[]);
float gauss_f(float a[][M], float b[], int n, float x[]);
void generate_f(float a[][M], float b[], int n);
void generate_d(double a[][M], double b[], int n);
int main()
{
    int i, j, n,lf=0,ld=0;
    double eps; // dok�adno�� rozwi�za�
    double a[M][M], b[M], x[M];
    /*S� to macierze do postaci r�wanania A*x = B */

    float a1[M][M], b1[M], x1[M];
    FILE* plik = fopen("wynik.txt", "w");

    /*nie chccia�o mi si� r�cnie tego wyznacza� - og�lnie ka�de r�wnanie powinno mie� rozwi�zanie =1 (Zbychu tak na filmie m�wi SPRAWD�CIE SAMI)
    Napisa�em zatem prosty kod by nie przekilkiwa� programu by wyznaczy� te wartosci tylko by to robi� za mnie program automatycznie- wydaje mi si�, �e 
    mo�ecie po prostu napisa� kod bez tego - napisa� w raporcie �eprzy dok�adno�ci 0.001 dla 10 r�wna� z doublem i 4 z floatem i wstawi� screeny do raportu, ze dziala dla 10 a 11 ju� nie,
    i dopisa� �e wysy�acie wersje finaln� programu z doublem (wyjebcie wtedy gauss_f i generate_f*/

    // Ochuj ta funkcja zle dziala jprdl - dla ma�ych b��d�w zajebi�cie ale potem si� robi� schody bo przy du�ych wato�ciach macierze wariuje
    printf("Podaj dokladnosc rozwiazan, ktore chcesz otrzymac");
    scanf("%lf", &eps);
    for (i = 1; i < M; i++)
    {
        bool f = false;
        bool d = false;
        generate_d(a, b, i);
        generate_f(a1, b1, i);
        gauss(a, b, i, x);
        gauss_f(a1, b1, i, x1);
        for (j = 1; j <= i; j++)
        {
            f = false;
            d = false;
            if (fabs(1-x[j])<eps)
                d = true;
            if (fabs(1 - x1[j]) < eps)
                f = true;
        }
        if (d == true)
            ld++;
        if (f == true)
            lf++;

    }
    printf("Rozwiazania rownania przy dokladnosci %lf otrzymujemy: \n przy uzyciu doubla dla %d rowanan \n przy uzyciu floata dla %d rownan\n ", eps, ld, lf);
    
    //dane zadania
    printf(" Podaj ilosc rownan n (maksimum to 30)=");
    scanf("%d", &n);
    // generowanie macierzy Hilberta- ogarnijcie wz�r czemu tak
    generate_d(a, b, n);
   
    /*wypisanie macierzy A i B w taki spos�b �e na ko�cu linijki pojawia Ci si� warto�� B 
    - bardzo brzydkie ale Nosal tak mia�
    */
    for (i = 1; i <= n; i++) 
    {
        for (j = 1; j <= n; j++) 
            printf("%lg ", a[i][j]);
        printf("%lg \n", b[i]);
    }

    double wyzanacznik = gauss(a, b, n, x);

    printf("\n wartosc wyzancznika macierzy  to %.64lf \n Wyniki zostaly zapisane do pliku wynik txt", wyzanacznik);
    //wypisanie wynik�w
    for (i = 1; i <= n; i++) 
        fprintf(plik,"%d %.64lf \n",i, x[i]);
    fclose(plik);
 
  
    return 0;
}
   
double gauss(double a[][M], double b[], int n, double x[])
{
    int i, j, l;
    double s;

    //eliminacja - zamieniemy A na macierz tr�jk�tn� - jak se to rzpisa�em i przemy�la�em to powinno dzia�a�

    for (i = 1; i < n; i++) 
    {
        for (l = i + 1; l <= n; l++) 
        {
            for (j = i + 1; j <= n; j++) 
                a[l][j] = a[l][j] - a[i][j] / a[i][i] * a[l][i];
            b[l] = b[l] - b[i] / a[i][i] * a[l][i];
        }
    }

    // rozwi�zanie uk�adu r�wna�

    x[n] = b[n] / a[n][n];

    for (i = n - 1; i > 0; i--) 
    {
        s = 0.;
        for (j = i + 1; j <= n; j++)
            s = s + x[j] * a[i][j];
        x[i] = (b[i] - s) / a[i][i];
    }

    double wyznacznik = 1;
    for(i=1;i<=n;i++)
    {
        wyznacznik *= a[i][i];
       
    }
    return wyznacznik;
}

float gauss_f(float a[][M], float b[], int n, float x[])
{
    int i, j, l;
    float s;

    //eliminacja - zamieniemy A na macierz tr�jk�tn� - jak se to rzpisa�em i przemy�la�em to powinno dzia�a�

    for (i = 1; i < n; i++)
    {
        for (l = i + 1; l <= n; l++)
        {
            for (j = i + 1; j <= n; j++)
                a[l][j] = a[l][j] - a[i][j] / a[i][i] * a[l][i];
            b[l] = b[l] - b[i] / a[i][i] * a[l][i];
        }
    }

    // rozwi�zanie uk�adu r�wna�

    x[n] = b[n] / a[n][n];

    for (i = n - 1; i > 0; i--)
    {
        s = 0.;
        for (j = i + 1; j <= n; j++)
            s = s + x[j] * a[i][j];
        x[i] = (b[i] - s) / a[i][i];
    }

    float wyznacznik = 1;
    for (i = 1; i <= n; i++)
    {
        wyznacznik *= a[i][i];
    }
    return wyznacznik;
}
void generate_d(double a[][M], double b[],int n)
{
    int i, j;
    for (i = 1; i <= n; i++)
    {
        b[i] = 0.;
        for (j = 1; j <= n; j++)
        {
            a[i][j] = 1. / (i + j - 1);

            b[i] = b[i] + a[i][j];
        }
    }
}
void generate_f(float a[][M], float b[], int n)
{
    int i, j;
    for (i = 1; i <= n; i++)
    {
        b[i] = 0.;
        for (j = 1; j <= n; j++)
        {
            a[i][j] = 1. / (i + j - 1);

            b[i] = b[i] + a[i][j];
        }
    }
}