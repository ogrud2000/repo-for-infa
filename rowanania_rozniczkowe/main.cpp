#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "winbgi2.h"

double eul(double x0, double y0, double h, double(*fun)(double, double));// metoda eulera
double rk4(double x0, double y0, double h, double(*fun)(double, double));// metoda runego-kutty 4tego stopnia
double fun(double t, double y);
double analitycznie(double t, double t0, double y0);
double lambda;

int main()
{
	 // y(2) = 1
	double t0 = 0;
	double y0 = 1;

	

	printf("Program rozwiazuje zagadnienie: y' = f(x,y)= lambda*y(x), y(x0) = y0 , gdzie lambda i y0 to liczby rzeczywiste\n Wprowadz x0i y0, oraz lambda: \n");
	scanf_s("%lf %lf %lf", &t0, &y0, &lambda);

	
	double dt = 1.;
	printf("Wprowadz dlugosc przedzialu czasowego (Pownien on byc mozliwie maly)\n");
	scanf_s("%lf",&dt);

	double t = t0; //od kiedy iterujemy
	double y = y0;

	//u�omna grafika i zapis do plika
	FILE* file = fopen("dane.txt", "w");
	graphics(600,600);
	
	scale(0., 0., 1., 5.);
	title("h", "epsilon", "Wykers bl�du rozwiazania w zaleznosci od kroku calkowania i metody.");
	int j = 0;
	for (; j <7; j++)
	{
		int n = pow(2, (double)j);
		double h = dt / (double)n;

		int i = 0;
		for (; i < n; i++)
		{
			t += h;
			y = eul(t, y, h, fun);
		}
		double eps_eul=fabs((analitycznie(t,t0,y0)-y)/y);//b��d eulera
		setcolor(1.);//czerwony
		circle(h, eps_eul, 5);//rysowanie czyli to co gumisie uwa�aj� za najbardziej bezsensowne

		y = y0;
		t = t0;
		for (i = 0; i < n; i++)
		{
			t += h;
			y = rk4(t, y, h, fun);
		}
		double eps_rk4=fabs((analitycznie(t, t0, y0) - y) / y);//b��d runnego- kuty
		setcolor(0.5);//zielony
		circle(h, eps_rk4, 5);

		fprintf(file,"%d %lf %lf %lf \n", n, h, eps_eul, eps_rk4);
	}
	

	fclose(file);
	wait();
	return 0;
}

double fun(double t,double y)
{
	return lambda * y;
}

double analitycznie(double t,double t0, double y0)
{
	return y0 * exp(lambda*(t-t0));
}

double eul(double t, double y, double h, double(*fun)(double, double))
{
	return y + h * fun(t,y);
}

double rk4(double x0, double y0, double h, double(*fun)(double, double))
{
	double y1;
	double k1, k2, k3, k4;
	k1 = h * fun(x0, y0);
	k2 = h * fun(x0 + h / 2., y0 + k1 / 2.);
	k3 = h * fun(x0 + h / 2., y0 + k2 / 2.);
	k4 = h * fun(x0 + h, y0 + k3);
	y1 = y0 + (k1 + 2. * k2 + 2. * k3 + k4) / 6.;
	return y1;
}