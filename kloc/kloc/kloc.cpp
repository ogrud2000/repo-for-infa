﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define MAXN 2

double mi, k, Pz, g, m;
void kloc(double t, double x[], double z[]);
void vrk4(double x0, double y0[], double h, void (*fun)(double, double*, double*), double y1[]);

int main()
{
// wartosc dt , n procent, xp , k , mi, Pz - sa podane przykladowo  - WARTO WPISAC TE SAME BO DLA TYCH PROGRAM ZADZIALA
    double x[MAXN], x0[MAXN]; //   x[0] - polozenie x[1] - predkosc
    double t0 = 0.;
    double dt;// dt=0.001 KROK CZASOWY - DANA METODY NUMERYCZNEJ
    double t;
    double tk = 100;// zmienna pomocnicza metody numerycznej - czas koncowy

    int i = 0;
    int  n; // n= 10  i-licznik wachniec n- ilosc wachniec
    double procent; // procent = 15
    
    double xp;// xp =30 maksymalne wychylenie po pierwszym cyklu

    //FILE* pl = fopen("plik.txt", "w");

    g = 9.81; //przyciąganie ziemskie
   // mi = 0.3; //wspolczynnik tarcia
   // Pz = 50; //N
   // k = 1000; //N/m
    
     
    printf("Podaj dane fizyczne zadania Pz , mi, k, xp - poczatkowe polozenie \n");
    scanf("%lf %lf %lf %lf",&Pz, &mi, &k, &xp);
    printf("Podaj dane numeryczne zadania dt - krok czasowy \n");
    scanf("%lf", &dt);
    printf("Podaj ilosc cykli n  i procent o ktory ma sie wygsic wahanie \n");
    scanf("%d %lf", &n, &procent);
  

    procent = procent / 100;

    //pierwsze m
    double m1 = 10;
    m = m1;
    t = t0;

    x0[1] = 0.0;//predosc
    x0[0] = xp;//polozenie
    //printf("siła sprezystosci poczkatkowa %lf tarcie poczotkowe %lf \n", k * xp / m, ((Pz / m) + g) * mi);
    while (t <= tk)
    {
        //fprintf(pl, "%.3lf %.5lf %.5lf\n", t, x0[1], x0[0]);
        vrk4(t, x0, dt, kloc, x);
        if (x0[1] * x[1] < 0.0)
        {
            //printf("%lf,%lf,%lf \n", t, x0[1], x0[0]);

            i++;
            if (i == 2*n)
                break;
        }
        x0[0] = x[0];
        x0[1] = x[1];
        t += dt;
    }

    double xk1 = fabs(x0[0]);
    //printf(" zrealizowane wychylenia w czasie %d \n", i);

    double m2 = 0.5;
    m = m2;
    t = t0;
    i = 0;

    x0[1] = 0.;//predosc
    x0[0] = xp;//polozenie

    //printf("siła sprezystosci poczkatkowa %lf tarcie poczotkowe %lf \n", k * xp / m, ((Pz / m) + g) * mi);
    while (t <= tk)
    {
        //fprintf(pl, "%.3lf %.5lf %.5lf\n", t, x0[1], x0[0]);
        vrk4(t, x0, dt, kloc, x);
        if (x0[1] * x[1] <= 0.0)
        {
            // printf("%lf,%lf \n", x0[1], x0[0]);
            i++;
            if (i == 2*n)
                break;
        }
        x0[0] = x[0];
        x0[1] = x[1];
        t += dt;
    }
    double xk2 = fabs(x0[0]);
    //printf(" zrealizowane wychylenia w czasie %d \n", i);


    printf("wychylenie koncowe 1:%lf\n wychylenie koncowe 2: %lf\n", xk1, xk2);
    double xk = (1 - procent) * xp;
    double m_szukane = m2 - (xk2 - xk) * ((m2 - m1) / ((xk2 - xk) - (xk1 - xk)));

    printf("szukana masa to %lf ", m_szukane);
    
    //fclose(pl);
    return 0;
}

void kloc(double t, double x[], double z[])
{

    z[0] = x[1];//predkosc, z[1]-przyspieszenie
    if (x[1] != 0)
        z[1] = -((k / m) * (x[0])) - x[1] / fabs(x[1]) * ((Pz / m) + g) * mi;
    else
        z[1] = -((k / m) * (x[0]));

}

void vrk4(double x0, double y0[], double h, void (*fun)(double, double*, double*), double y1[])
{
    int i;
    double k1[MAXN];
    double k2[MAXN];
    double k3[MAXN];
    double k4[MAXN];
    double ytmp[MAXN];
    fun(x0, y0, k1);
    for (i = 0; i < MAXN; ++i)
    {
        k1[i] *= h;
        ytmp[i] = y0[i] + k1[i] / 2.0;
    }

    fun(x0 + h / 2.0, ytmp, k2);

    for (i = 0; i < MAXN; ++i)
    {
        k2[i] *= h;
        ytmp[i] = y0[i] + k2[i] / 2.0;
    }

    fun(x0 + h / 2.0, ytmp, k3);

    for (i = 0; i < MAXN; ++i)
    {
        k3[i] *= h;
        ytmp[i] = y0[i] + k3[i];
    }

    fun(x0 + h, ytmp, k4);

    for (i = 0; i < MAXN; ++i)
        k4[i] *= h;

    for (i = 0; i < MAXN; ++i)
        y1[i] = y0[i] + (k1[i] + 2. * k2[i] + 2. * k3[i] + k4[i]) / 6.;
}